/**
 * Licensed under MIT License
 * Copyright (c) 2017 Bernhard Grünewaldt
 */
const KartoffelstampfConstants = {
    uploadDir: '/u'
};

module.exports = KartoffelstampfConstants;